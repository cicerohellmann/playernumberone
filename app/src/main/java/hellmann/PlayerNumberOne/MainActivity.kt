package hellmann.PlayerNumberOne

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.moshi.Moshi
import hellmann.PlayerNumberOne.ApexModels.Player
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var serviceURL = "http://api.mozambiquehe.re/bridge?version=2&platform=PC&player=Zek1MOFO&auth=9HvNVCDOakGXdMMpmaB1"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sendRequest();
    }
    private fun sendRequest() {
        val oRequest = object : StringRequest(Request.Method.POST, serviceURL, Response.Listener { response ->
            parseJSON(response)
        }, Response.ErrorListener { error ->
            Toast.makeText(applicationContext, "Error: " + (error.toString()), Toast.LENGTH_SHORT).show();
        }) {
        }
        val requestQ = Volley.newRequestQueue(this)
        requestQ.add(oRequest)
    }

    fun parseJSON(json: String) {
        val moshi = Moshi.Builder().build()
        val jsonAdapter = moshi.adapter<Player>(Player::class.java)
        var player: Player? = null

        try {
            player = jsonAdapter.fromJson(json)
            playerName.setText(player?.global?.name)
            playerLevel.setText(player?.global?.level.toString())
        } catch (e: Exception) {
            e.printStackTrace()
        }
//        Toast.makeText(applicationContext, "Test: " + player, Toast.LENGTH_SHORT).show();
    }

}
