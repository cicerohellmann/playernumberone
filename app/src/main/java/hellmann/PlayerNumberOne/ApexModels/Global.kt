package hellmann.PlayerNumberOne.ApexModels

data class Global(
    val battlepass: Battlepass,
    val internalUpdateCount: Int,
    val level: Int,
    val name: String,
    val platform: String,
    val rank: Rank,
    val toNextLevelPercent: Int,
    val uid: Long
)