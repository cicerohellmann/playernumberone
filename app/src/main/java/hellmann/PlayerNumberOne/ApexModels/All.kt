package hellmann.PlayerNumberOne.ApexModels

data class All(
    val Bangalore: Bangalore,
    val Bloodhound: Bloodhound,
    val Caustic: Caustic,
    val Gibraltar: Gibraltar,
    val Lifeline: Lifeline,
    val Mirage: Mirage,
    val Octane: Octane,
    val Pathfinder: Pathfinder,
    val Wattson: Wattson,
    val Wraith: Wraith
)