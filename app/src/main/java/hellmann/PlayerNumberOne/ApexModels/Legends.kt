package hellmann.PlayerNumberOne.ApexModels

data class Legends(
    val all: All,
    val selected: Selected
)