package hellmann.PlayerNumberOne.ApexModels

data class Total(
    val kd: Kd,
    val kills: Kills,
    val wins_season_2: WinsSeason2
)