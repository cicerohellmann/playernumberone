package hellmann.PlayerNumberOne.ApexModels

data class Player(
    val global: Global,
    val legends: Legends,
    val mozambiquehere_internal: MozambiquehereInternal,
    val realtime: Realtime,
    val total: Total
)