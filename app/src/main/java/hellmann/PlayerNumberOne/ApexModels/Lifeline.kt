package hellmann.PlayerNumberOne.ApexModels

data class Lifeline(
    val ImgAssets: ImgAssets,
    val kills: Int
)