package hellmann.PlayerNumberOne.ApexModels

data class ImgAssets(
    val banner: String,
    val icon: String
)