package hellmann.PlayerNumberOne.ApexModels

data class Kd(
    val name: String,
    val value: Int
)