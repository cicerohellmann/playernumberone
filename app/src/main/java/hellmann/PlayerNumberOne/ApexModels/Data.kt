package hellmann.PlayerNumberOne.ApexModels

data class Data(
    val kills: Kills,
    val wins_season_2: WinsSeason2
)