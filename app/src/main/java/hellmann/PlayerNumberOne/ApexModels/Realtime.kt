package hellmann.PlayerNumberOne.ApexModels

data class Realtime(
    val canJoin: Int,
    val isInGame: Int,
    val isOnline: Int,
    val lobbyState: String,
    val partyFull: Int,
    val selectedLegend: String
)