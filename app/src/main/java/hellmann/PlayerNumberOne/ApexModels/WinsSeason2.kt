package hellmann.PlayerNumberOne.ApexModels

data class WinsSeason2(
    val key: String,
    val name: String,
    val value: Int
)