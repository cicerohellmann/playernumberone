package hellmann.PlayerNumberOne.ApexModels

data class Rank(
    val rankDiv: Int,
    val rankImg: String,
    val rankName: String,
    val rankScore: Int
)