package hellmann.PlayerNumberOne.ApexModels

data class Pathfinder(
    val ImgAssets: ImgAssets,
    val data: Data
)