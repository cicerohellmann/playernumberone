package hellmann.PlayerNumberOne.ApexModels

data class Kills(
    val key: String,
    val name: String,
    val value: Int
)