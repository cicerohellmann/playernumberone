package hellmann.PlayerNumberOne.ApexModels

data class MozambiquehereInternal(
    val APIAccessType: Any,
    val ClusterID: String,
    val claimedBy: String
)